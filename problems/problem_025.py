# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
     if bool(values):
        total = 0
        for num in values:
            total += num
        return total
     else:
         return None

print(calculate_sum([1,2,3,4,5]))
print(calculate_sum([]))
