# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    condition_one = x >= rect_x
    condition_two = y >= rect_y
    condtion_thee = x <= rect_x + rect_y
    condition_four = y <= rect_y + rect_x

    if condition_one and condition_two and condtion_thee and condition_four:
        return True
    return False

print(is_inside_bounds(8, 9, 5, 6, 10, 12))
print(is_inside_bounds(20, 9, 5, 6, 10, 12))
