# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) == 1:
        return None
    if bool(values):
        sorted_list = sorted(values)
        return sorted_list[len(sorted_list) - 2]
    else:
        return None


print(find_second_largest([1]))
print(find_second_largest([]))
print("The second largest value is",find_second_largest([2,7,8,100,60,23,52,1]))
