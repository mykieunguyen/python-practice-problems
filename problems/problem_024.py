# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
   if bool(values):
        total = 0
        avg = 0
        for num in values:
            total += num
        avg = total / len(values)
        return avg
   else:
       return None


print(calculate_average([1,2,3,4,5,6]))
print(calculate_average([]))
