# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    pasta_list = ["flour", "eggs", "oil"]

    if ingredients in pasta_list:
        return True
    return False

print(can_make_pasta("flour"))
print(can_make_pasta("whatever"))
