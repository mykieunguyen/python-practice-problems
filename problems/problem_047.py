# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

# def check_password(password):
#     last_index = len(password) - 1

# #   * It must have six or more characters in it
#     if last_index < 5 or last_index > 12:
#         return False;

# #   * It must have at least one lowercase letter (a-z)
#     for index,char in enumerate(password):
#         if char.islower():
#             break
#         if index == last_index:
#             if not char.islower():
#                 return False;

# #   * It must have at least one uppercase letter (A-Z)
#     for index,char in enumerate(password):
#         if char.isupper():
#             break
#         if index == last_index:
#             if not char.isupper():
#                 return False;

# #   * It must have at least one digit (0-9)
#     for index,char in enumerate(password):
#         if char.isdigit():
#             break
#         if index == last_index:
#             if not char.isdigit():
#                 return False;

# #   * It must have at least one special character $, !, or @
#     for index,char in enumerate(password):
#         if char in "$!@":
#             break
#         if index == last_index:
#             if char not in "$!@":
#                 return False;

#     return True;


# print(check_password("HelloWorl1!"))


def check_password(any_string):
    lower_case_flag = False
    upper_case_flag = False
    has_number_flag = False
    special_char_flag = False

    if len(any_string) > 12 or len(any_string) < 6:
        return False;

    for char in any_string:
        if char.islower():
            lower_case_flag = True
        if char.isupper():
            upper_case_flag = True
        if char.isdigit():
            has_number_flag = True
        if char in "!@$":
            special_char_flag = True

    return (lower_case_flag and upper_case_flag and has_number_flag and special_char_flag)

print(check_password("HelloWorld1!"))
