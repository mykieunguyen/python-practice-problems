# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
import math

def halve_the_list(a_list):
    lst_length = len(a_list)

    # if odd
    if lst_length % 2:
        odd_lst = math.ceil((lst_length / 2))
        return a_list[:odd_lst], a_list[odd_lst:lst_length]
    #if even
    else:
        even_lst = lst_length // 2
        return a_list[:even_lst], a_list[even_lst:lst_length]

print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3, 4, 5, 6, 7]))
