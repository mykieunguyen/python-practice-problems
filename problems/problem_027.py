# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if bool(values):
        max = 0
        for num in values:
            if num >= max:
                max = num
        return max
    else:
         return None


print(max_in_list([2,6,4,23,423]))
print(max_in_list([]))
