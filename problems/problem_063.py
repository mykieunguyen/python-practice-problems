# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    new_string = ''

    for letter in word:
        unicode_form = ord(letter)

        if unicode_form == 122:
            new_string += chr(97)
        else:
            new_string += chr(unicode_form + 1)

    return new_string


print(shift_letters("import"))
print(shift_letters("ABBA"))
print(shift_letters("Kala"))
print(shift_letters("zap"))
